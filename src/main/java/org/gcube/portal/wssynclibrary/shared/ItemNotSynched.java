package org.gcube.portal.wssynclibrary.shared;


// TODO: Auto-generated Javadoc
/**
 * The Class ItemNotSynched.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * Feb 8, 2018
 */
public class ItemNotSynched extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3965350221961538838L;
	
	/**
	 * Instantiates a new item not synched.
	 */
	public ItemNotSynched() {
		super();
	}

	/**
	 * Instantiates a new item not synched.
	 *
	 * @param arg0 the arg 0
	 */
	public ItemNotSynched(String arg0) {
		super(arg0);
	}

}
