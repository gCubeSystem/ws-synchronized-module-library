package org.gcube.portal.wssynclibrary.shared.thredds;

import java.io.Serializable;

/**
 * The Class ThSyncElementInfo.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         May 13, 2021
 */
public class ThSyncElementInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6248465200945019743L;

	private Sync_Status syncStatus;

	private ThStepReport stepReport;

	private String lastupdateTime;

	/**
	 * Instantiates a new th sync element info.
	 */
	public ThSyncElementInfo() {

	}

	/**
	 * Instantiates a new th sync element info.
	 *
	 * @param syncStatus the sync status
	 */
	public ThSyncElementInfo(Sync_Status syncStatus) {
		super();
		this.syncStatus = syncStatus;
	}

	/**
	 * Gets the sync status.
	 *
	 * @return the sync status
	 */
	public Sync_Status getSyncStatus() {
		return syncStatus;
	}

	/**
	 * Sets the sync status.
	 *
	 * @param syncStatus the new sync status
	 */
	public void setSyncStatus(Sync_Status syncStatus) {
		this.syncStatus = syncStatus;
	}

	public ThStepReport getStepReport() {
		return stepReport;
	}

	public void setStepReport(ThStepReport stepReport) {
		this.stepReport = stepReport;
	}

	public String getLastupdateTime() {
		return lastupdateTime;
	}

	public void setLastupdateTime(String lastupdateTime) {
		this.lastupdateTime = lastupdateTime;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ThSyncElementInfo [syncStatus=");
		builder.append(syncStatus);
		builder.append(", stepReport=");
		builder.append(stepReport);
		builder.append(", lastupdateTime=");
		builder.append(lastupdateTime);
		builder.append("]");
		return builder.toString();
	}

}