package org.gcube.portal.wssynclibrary.shared.thredds;

import java.io.Serializable;

/**
 * The Class ThSyncFolderDescriptor.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 * May 13, 2021
 */
public class ThSyncFolderDescriptor implements Serializable {

	private static final long serialVersionUID = 4134777763175272691L;

	private String folderId;

	private String folderPath;

	private ThSynchFolderConfiguration configuration;

	private boolean isLocked = false;

	private ThSyncElementInfo elementInfo;

	private ThProcessDescriptor localProcessDescriptor = null;

	/**
	 * Instantiates a new s sync folder descriptor.
	 */
	public ThSyncFolderDescriptor() {

	}

	/**
	 * Instantiates a new th sync folder descriptor.
	 *
	 * @param folderId               the folder id
	 * @param folderPath             the folder path
	 * @param configuration          the configuration
	 * @param isLocked               the is locked
	 * @param elementInfo            the element info
	 * @param localProcessDescriptor the local process descriptor
	 */
	public ThSyncFolderDescriptor(String folderId, String folderPath, ThSynchFolderConfiguration configuration,
			boolean isLocked, ThSyncElementInfo elementInfo, ThProcessDescriptor localProcessDescriptor) {
		super();
		this.folderId = folderId;
		this.folderPath = folderPath;
		this.configuration = configuration;
		this.isLocked = isLocked;
		this.elementInfo = elementInfo;
		this.localProcessDescriptor = localProcessDescriptor;
	}

	/**
	 * Gets the folder id.
	 *
	 * @return the folder id
	 */
	public String getFolderId() {
		return folderId;
	}

	/**
	 * Sets the folder id.
	 *
	 * @param folderId the new folder id
	 */
	public void setFolderId(String folderId) {
		this.folderId = folderId;
	}

	/**
	 * Gets the folder path.
	 *
	 * @return the folder path
	 */
	public String getFolderPath() {
		return folderPath;
	}

	/**
	 * Sets the folder path.
	 *
	 * @param folderPath the new folder path
	 */
	public void setFolderPath(String folderPath) {
		this.folderPath = folderPath;
	}

	/**
	 * Gets the configuration.
	 *
	 * @return the configuration
	 */
	public ThSynchFolderConfiguration getConfiguration() {
		return configuration;
	}

	/**
	 * Sets the configuration.
	 *
	 * @param configuration the new configuration
	 */
	public void setConfiguration(ThSynchFolderConfiguration configuration) {
		this.configuration = configuration;
	}

	/**
	 * Checks if is locked.
	 *
	 * @return true, if is locked
	 */
	public boolean isLocked() {
		return isLocked;
	}

	/**
	 * Sets the locked.
	 *
	 * @param isLocked the new locked
	 */
	public void setLocked(boolean isLocked) {
		this.isLocked = isLocked;
	}

	/**
	 * Gets the local process descriptor.
	 *
	 * @return the local process descriptor
	 */
	public ThProcessDescriptor getLocalProcessDescriptor() {
		return localProcessDescriptor;
	}

	/**
	 * Sets the local process descriptor.
	 *
	 * @param localProcessDescriptor the new local process descriptor
	 */
	public void setLocalProcessDescriptor(ThProcessDescriptor localProcessDescriptor) {
		this.localProcessDescriptor = localProcessDescriptor;
	}

	/**
	 * Gets the element info.
	 *
	 * @return the element info
	 */
	public ThSyncElementInfo getElementInfo() {
		return elementInfo;
	}

	/**
	 * Sets the element info.
	 *
	 * @param elementInfo the new element info
	 */
	public void setElementInfo(ThSyncElementInfo elementInfo) {
		this.elementInfo = elementInfo;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ThSyncFolderDescriptor [folderId=");
		builder.append(folderId);
		builder.append(", folderPath=");
		builder.append(folderPath);
		builder.append(", configuration=");
		builder.append(configuration);
		builder.append(", isLocked=");
		builder.append(isLocked);
		builder.append(", elementInfo=");
		builder.append(elementInfo);
		builder.append(", localProcessDescriptor=");
		builder.append(localProcessDescriptor);
		builder.append("]");
		return builder.toString();
	}

}
