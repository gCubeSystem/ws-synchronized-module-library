package org.gcube.portal.wssynclibrary.shared.thredds;

import java.io.Serializable;

/**
 * The Class ThSynchFolderConfiguration.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it Feb 8, 2018
 */
public class ThSynchFolderConfiguration implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5807533629170443212L;

	/** The remote path. */
	private String remotePath;

	/** The filter. */
	private String filter;

	private String targetContext;

	/** The to create catalog name. */
	private String toCreateCatalogName;

	/** The remote persistence. */
	private String remotePersistence = "thredds";

	private String rootFolderId;

	/**
	 * Instantiates a new th synch folder configuration.
	 */
	public ThSynchFolderConfiguration() {

	}

	/**
	 * Instantiates a new th synch folder configuration.
	 *
	 * @param remotePath          the remote path
	 * @param filter              the filter
	 * @param targetContext       the target context
	 * @param toCreateCatalogName the to create catalog name
	 * @param remotePersistence   the remote persistence
	 * @param rootFolderId        the root folder id
	 */
	public ThSynchFolderConfiguration(String remotePath, String filter, String targetContext,
			String toCreateCatalogName, String remotePersistence, String rootFolderId) {
		super();
		this.remotePath = remotePath;
		this.filter = filter;
		this.targetContext = targetContext;
		this.toCreateCatalogName = toCreateCatalogName;
		this.remotePersistence = remotePersistence;
		this.rootFolderId = rootFolderId;
	}

	/**
	 * Gets the root folder id.
	 *
	 * @return the rootFolderId
	 */
	public String getRootFolderId() {

		return rootFolderId;
	}

	/**
	 * Sets the root folder id.
	 *
	 * @param rootFolderId the rootFolderId to set
	 */
	public void setRootFolderId(String rootFolderId) {

		this.rootFolderId = rootFolderId;
	}

	/**
	 * Gets the remote path.
	 *
	 * @return the remote path
	 */
	public String getRemotePath() {
		return remotePath;
	}

	/**
	 * Sets the remote path.
	 *
	 * @param remotePath the new remote path
	 */
	public void setRemotePath(String remotePath) {
		this.remotePath = remotePath;
	}

	/**
	 * Gets the filter.
	 *
	 * @return the filter
	 */
	public String getFilter() {
		return filter;
	}

	/**
	 * Sets the filter.
	 *
	 * @param filter the new filter
	 */
	public void setFilter(String filter) {
		this.filter = filter;
	}

	/**
	 * Gets the target context.
	 *
	 * @return the target context
	 */
	public String getTargetContext() {
		return targetContext;
	}

	/**
	 * Sets the target context.
	 *
	 * @param targetContext the new target context
	 */
	public void setTargetContext(String targetContext) {
		this.targetContext = targetContext;
	}

	/**
	 * Gets the to create catalog name.
	 *
	 * @return the to create catalog name
	 */
	public String getToCreateCatalogName() {
		return toCreateCatalogName;
	}

	/**
	 * Sets the to create catalog name.
	 *
	 * @param toCreateCatalogName the new to create catalog name
	 */
	public void setToCreateCatalogName(String toCreateCatalogName) {
		this.toCreateCatalogName = toCreateCatalogName;
	}

	/**
	 * Gets the remote persistence.
	 *
	 * @return the remote persistence
	 */
	public String getRemotePersistence() {
		return remotePersistence;
	}

	/**
	 * Sets the remote persistence.
	 *
	 * @param remotePersistence the new remote persistence
	 */
	public void setRemotePersistence(String remotePersistence) {
		this.remotePersistence = remotePersistence;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ThSynchFolderConfiguration [remotePath=");
		builder.append(remotePath);
		builder.append(", filter=");
		builder.append(filter);
		builder.append(", targetContext=");
		builder.append(targetContext);
		builder.append(", toCreateCatalogName=");
		builder.append(toCreateCatalogName);
		builder.append(", remotePersistence=");
		builder.append(remotePersistence);
		builder.append(", rootFolderId=");
		builder.append(rootFolderId);
		builder.append("]");
		return builder.toString();
	}

}
