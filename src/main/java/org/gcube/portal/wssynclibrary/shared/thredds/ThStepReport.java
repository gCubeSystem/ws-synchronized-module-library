package org.gcube.portal.wssynclibrary.shared.thredds;

import java.io.Serializable;

/**
 * The Class ThStepReport.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         May 18, 2021
 */
public class ThStepReport implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1678408521458644765L;
	private String stepReportStatus;

	/**
	 * Instantiates a new th step report.
	 */
	public ThStepReport() {

	}

	/**
	 * Instantiates a new th step report.
	 *
	 * @param stepReportStatus the step report status
	 */
	public ThStepReport(String stepReportStatus) {
		this.stepReportStatus = stepReportStatus;
	}

	/**
	 * Gets the step report status.
	 *
	 * @return the step report status
	 */
	public String getStepReportStatus() {
		return stepReportStatus;
	}

	/**
	 * Sets the step report status.
	 *
	 * @param stepReportStatus the new step report status
	 */
	public void setStepReportStatus(String stepReportStatus) {
		this.stepReportStatus = stepReportStatus;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ThStepReport [stepReportStatus=");
		builder.append(stepReportStatus);
		builder.append("]");
		return builder.toString();
	}

}
