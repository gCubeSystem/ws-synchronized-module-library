package org.gcube.portal.wssynclibrary;

/**
 * The Interface DoConnectRepository.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it Feb 14, 2018
 */
public interface DoConnectRepository {

	/**
	 * Inits the repository.
	 *
	 * @return true, if successful
	 * @throws Exception
	 */
	Boolean initRepository() throws Exception;

	/**
	 * Shutdown repository.
	 *
	 * @return true, if successful
	 */
	Boolean shutDownRepository();

}
