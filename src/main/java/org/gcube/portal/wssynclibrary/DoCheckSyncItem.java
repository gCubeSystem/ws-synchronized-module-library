/*
 *
 */
package org.gcube.portal.wssynclibrary;

import org.gcube.portal.wssynclibrary.shared.ItemNotSynched;
import org.gcube.usecases.ws.thredds.faults.WorkspaceInteractionException;
import org.gcube.usecases.ws.thredds.faults.WorkspaceNotSynchedException;

/**
 * The Interface DoCheckSyncItem.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it Feb 8, 2018
 * @param <T> the generic type
 */
public interface DoCheckSyncItem<T> {

	/**
	 * Check item synched.
	 *
	 * @param itemId the item id
	 * @return the t
	 * @throws ItemNotSynched the item not synched
	 * @throws Exception      the exception
	 */
	T checkItemSynched(String itemId) throws ItemNotSynched, Exception;

	/**
	 * Gets the configuration.
	 *
	 * @param itemId the item id
	 * @return the configuration
	 * @throws WorkspaceInteractionException the workspace interaction exception
	 * @throws WorkspaceNotSynchedException  the workspace not synched exception
	 * @throws Exception                     the exception
	 */
	T getConfiguration(String itemId) throws WorkspaceInteractionException, WorkspaceNotSynchedException, Exception;
}
