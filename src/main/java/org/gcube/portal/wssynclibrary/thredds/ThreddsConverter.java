package org.gcube.portal.wssynclibrary.thredds;

import java.util.function.Function;

import org.gcube.portal.wssynclibrary.shared.thredds.Status;
import org.gcube.portal.wssynclibrary.shared.thredds.Sync_Status;
import org.gcube.portal.wssynclibrary.shared.thredds.ThCatalogueBean;
import org.gcube.portal.wssynclibrary.shared.thredds.ThProcessDescriptor;
import org.gcube.portal.wssynclibrary.shared.thredds.ThProcessStatus;
import org.gcube.portal.wssynclibrary.shared.thredds.ThStepReport;
import org.gcube.portal.wssynclibrary.shared.thredds.ThSyncElementInfo;
import org.gcube.portal.wssynclibrary.shared.thredds.ThSyncFolderDescriptor;
import org.gcube.portal.wssynclibrary.shared.thredds.ThSynchFolderConfiguration;
import org.gcube.usecases.ws.thredds.engine.impl.ProcessDescriptor;
import org.gcube.usecases.ws.thredds.engine.impl.ProcessStatus;
import org.gcube.usecases.ws.thredds.model.SyncFolderDescriptor;
import org.gcube.usecases.ws.thredds.model.SynchFolderConfiguration;
import org.gcube.usecases.ws.thredds.model.SynchronizedElementInfo;
import org.gcube.usecases.ws.thredds.model.SynchronizedElementInfo.SynchronizationStatus;
import org.gcube.usecases.ws.thredds.model.gui.CatalogBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class ThreddsConverter.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it Feb 14, 2018
 *         updated May 13, 2021
 */
public class ThreddsConverter {

	public static final String THREDDS_FILTER_NC_NCML_ASC = "*.nc,*.ncml,*.asc";

	private static Logger logger = LoggerFactory.getLogger(ThreddsConverter.class);

	/** The to S sync folder descriptor. */
	public static Function<SyncFolderDescriptor, ThSyncFolderDescriptor> toThSyncFolderDescriptor = new Function<SyncFolderDescriptor, ThSyncFolderDescriptor>() {

		public ThSyncFolderDescriptor apply(SyncFolderDescriptor t) {
			ThSyncFolderDescriptor mySync = new ThSyncFolderDescriptor();
			if (t == null) {
				logger.info("Input " + SyncFolderDescriptor.class.getSimpleName() + " is null, returning empty "
						+ ThSyncFolderDescriptor.class.getSimpleName());
				return mySync;
			}

			mySync.setFolderId(t.getFolderId());
			mySync.setFolderPath(t.getFolderPath());
			mySync.setLocked(t.getIsLocked());
			Sync_Status theSyncStatus = toSyncStatus.apply(t.getStatus());
			mySync.setElementInfo(new ThSyncElementInfo(theSyncStatus));
			ThProcessDescriptor localProcessDescriptor = toThProcessDescriptor.apply(t.getLocalProcessDescriptor());
			mySync.setLocalProcessDescriptor(localProcessDescriptor);
			ThSynchFolderConfiguration configuration = toThSynchFolderConfiguration.apply(t.getConfiguration());
			mySync.setConfiguration(configuration);

			return mySync;
		}
	};

	/** The to S sync folder descriptor. */
	public static Function<ThSyncFolderDescriptor, SyncFolderDescriptor> toSyncFolderDescriptor = new Function<ThSyncFolderDescriptor, SyncFolderDescriptor>() {

		public SyncFolderDescriptor apply(ThSyncFolderDescriptor t) {

			if (t == null) {
				logger.warn("Input " + ThSyncFolderDescriptor.class.getSimpleName() + " is null, returning null");
				return null;
			}

			SynchFolderConfiguration configuration = toSynchFolderConfiguration.apply(t.getConfiguration());
			SynchronizedElementInfo sei = toSynchronizedElementInfo.apply(t.getElementInfo());
			return new SyncFolderDescriptor(sei.getStatus(), t.getFolderId(), t.getFolderPath(), configuration,
					t.isLocked());
		}
	};

	/** The to synch folder configuration. */
	public static Function<ThSynchFolderConfiguration, SynchFolderConfiguration> toSynchFolderConfiguration = new Function<ThSynchFolderConfiguration, SynchFolderConfiguration>() {

		public SynchFolderConfiguration apply(ThSynchFolderConfiguration t) {

			if (t == null) {
				logger.warn("Input " + ThSynchFolderConfiguration.class.getSimpleName() + " is null, returning null");
				return null;
			}

			String filter = t.getFilter() != null && !t.getFilter().isEmpty() ? t.getFilter()
					: THREDDS_FILTER_NC_NCML_ASC;
			return new SynchFolderConfiguration(t.getRemotePath(), filter, t.getTargetContext(),
					t.getToCreateCatalogName(), t.getRootFolderId());
		}
	};

	/** The to S synch folder configuration. */
	public static Function<SynchFolderConfiguration, ThSynchFolderConfiguration> toThSynchFolderConfiguration = new Function<SynchFolderConfiguration, ThSynchFolderConfiguration>() {

		public ThSynchFolderConfiguration apply(SynchFolderConfiguration t) {
			ThSynchFolderConfiguration mySync = new ThSynchFolderConfiguration();
			if (t == null) {
				logger.info("Input " + SynchFolderConfiguration.class.getSimpleName() + " is null, returning empty "
						+ ThSynchFolderConfiguration.class.getSimpleName());
				return mySync;
			}

			mySync.setFilter(t.getFilter());
			mySync.setRemotePath(t.getRemotePath());
			mySync.setRemotePersistence(t.getRemotePersistence());
			mySync.setTargetContext(t.getTargetContext());
			mySync.setToCreateCatalogName(t.getToCreateCatalogName());
			mySync.setRootFolderId(t.getRootFolderId());
			return mySync;
		}
	};

	/** The to S sync folder descriptor. */
	public static Function<ProcessDescriptor, ThProcessDescriptor> toThProcessDescriptor = new Function<ProcessDescriptor, ThProcessDescriptor>() {

		public ThProcessDescriptor apply(ProcessDescriptor t) {
			ThProcessDescriptor mySync = new ThProcessDescriptor();
			if (t == null) {
				logger.info("Input " + ProcessDescriptor.class.getSimpleName() + " is null, returning empty "
						+ ThProcessDescriptor.class.getSimpleName());
				return mySync;
			}

			mySync.setFolderId(t.getFolderId());
			mySync.setFolderPath(t.getFolderPath());
			mySync.setProcessId(t.getProcessId());
			ThSynchFolderConfiguration fc = toThSynchFolderConfiguration.apply(t.getSynchConfiguration());
			mySync.setSynchConfiguration(fc);
			return mySync;
		}
	};

	/** The to th catalogue bean. */
	public static Function<CatalogBean, ThCatalogueBean> toThCatalogueBean = new Function<CatalogBean, ThCatalogueBean>() {

		@Override
		public ThCatalogueBean apply(CatalogBean t) {

			if (t == null)
				return null;

			return new ThCatalogueBean(t.getName(), t.getPath(), t.getIsDefault());
		}
	};

	/** The to th process status. */
	public static Function<ProcessStatus, ThProcessStatus> toThProcessStatus = new Function<ProcessStatus, ThProcessStatus>() {

		public ThProcessStatus apply(ProcessStatus t) {
			ThProcessStatus mySync = new ThProcessStatus();
			if (t == null) {
				logger.info("Input " + ProcessStatus.class.getSimpleName() + " is null, returning empty "
						+ ThProcessStatus.class.getSimpleName());
				return mySync;
			}

			mySync.setCurrentMessage(t.getCurrentMessage());
			mySync.setPercentCompleted(t.getPercent());

			if (t.getErrorCount() != null)
				mySync.setErrorCount(t.getErrorCount().get());
			if (t.getLogBuilder() != null)
				mySync.setLogBuilder(t.getLogBuilder().toString());
			if (t.getQueuedTransfers() != null)
				mySync.setQueuedTransfers(t.getQueuedTransfers().get());
			if (t.getServedTransfers() != null)
				mySync.setServedTransfers(t.getServedTransfers().get());

			if (t.getStatus() != null) {

				switch (t.getStatus()) {
				case COMPLETED:
					mySync.setStatus(Status.COMPLETED);
					break;
				case INITIALIZING:
					mySync.setStatus(Status.INITIALIZING);
					break;
				case ONGOING:
					mySync.setStatus(Status.ONGOING);
					break;
				case WARNINGS:
					mySync.setStatus(Status.WARNINGS);
					break;
				case STOPPED:
					mySync.setStatus(Status.STOPPED);
					break;

				default:
					break;
				}
			}
			return mySync;
		}
	};

	public static Function<SynchronizedElementInfo, ThSyncElementInfo> toThSyncElementInfo = new Function<SynchronizedElementInfo, ThSyncElementInfo>() {

		public ThSyncElementInfo apply(SynchronizedElementInfo sei) {

			if (sei == null) {
				logger.info("Input " + SynchronizedElementInfo.class.getSimpleName() + " is null, returning null "
						+ ThSyncElementInfo.class.getSimpleName());
				return null;
			}

			ThSyncElementInfo toReturn = new ThSyncElementInfo();
			Sync_Status theSyncStatus = toSyncStatus.apply(sei.getStatus());
			toReturn.setSyncStatus(theSyncStatus);
			toReturn.setLastupdateTime(sei.getLastupdateTime());
			String lastSyncStatus = sei.getLastSynchronizationStatus() != null
					? sei.getLastSynchronizationStatus().name()
					: null;
			toReturn.setStepReport(new ThStepReport(lastSyncStatus));
			return toReturn;
		}
	};

	public static Function<SynchronizationStatus, Sync_Status> toSyncStatus = new Function<SynchronizationStatus, Sync_Status>() {

		public Sync_Status apply(SynchronizationStatus synchronizationStatus) {

			if (synchronizationStatus == null) {
				logger.info("Input " + SynchronizationStatus.class.getSimpleName() + " is null, returning null "
						+ Sync_Status.class.getSimpleName());
				return null;
			}

			if (synchronizationStatus != null) {
				switch (synchronizationStatus) {
				case UP_TO_DATE:
					return Sync_Status.UP_TO_DATE;
				case OUTDATED_WS:
					return Sync_Status.OUTDATED_WS;
				case OUTDATED_REMOTE:
					return Sync_Status.OUTDATED_REMOTE;
				default:
					break;
				}
			}
			return null;
		}
	};

	public static Function<ThSyncElementInfo, SynchronizedElementInfo> toSynchronizedElementInfo = new Function<ThSyncElementInfo, SynchronizedElementInfo>() {

		public SynchronizedElementInfo apply(ThSyncElementInfo sei) {

			if (sei == null) {
				logger.info("Input " + ThSyncElementInfo.class.getSimpleName() + " is null, returning null "
						+ SynchronizedElementInfo.class.getSimpleName());
				return null;
			}

			Sync_Status status = sei.getSyncStatus();
			SynchronizationStatus theStatus = null;
			if (status != null) {

				switch (status) {
				case UP_TO_DATE:
					theStatus = SynchronizedElementInfo.SynchronizationStatus.UP_TO_DATE;
					break;
				case OUTDATED_WS:
					theStatus = SynchronizedElementInfo.SynchronizationStatus.OUTDATED_WS;
					break;
				case OUTDATED_REMOTE:
					theStatus = SynchronizedElementInfo.SynchronizationStatus.OUTDATED_REMOTE;
				default:
					break;
				}
			}
			return new SynchronizedElementInfo(theStatus);
		}
	};
}
