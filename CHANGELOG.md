
# Changelog

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [v1.5.1] - 2021-07-20

Moved to maven-portal-bom 3.6.3
Just to include new version of ws-thredds

## [v1.5.0] - 2021-05-07

[#21374] Moved to ws-thredds 1.x
Moved to maven-portal-bom 3.6.2


## [v1.4.0] - 2021-03-10

[#20927] Migrated to git/jenkins

Using the ws-thredds dependency [0.1.0, 1.0.0-SNAPSHOT)


## [v1.3.0] - 2019-09-25

[#17348] Migrate ws-thredds-sync components to SHUB


## [v1.2.0] - 2019-06-31

Only changed the call getPropertMap -> getMetadata.getMap


## [v1.1.0] - 2018-09-04

[#12909] Reading the "WS-SYNCH.SYNCH-STATUS" property from StorageHub instead of HL


## [v1.0.1] - 2018-09-04

[#12302] Fix ws-thredds dependency on workspace


## [v1.0.0] - 2018-03-16

First Release
